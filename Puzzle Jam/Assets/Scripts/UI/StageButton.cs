﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageButton : MonoBehaviour
{
    private void Start()
    {
        var stageNumber = int.Parse(GetComponentInChildren<Text>().text);

        GetComponent<Button>().onClick.AddListener(() => Statistics.Instance.currentLevel = stageNumber - 1);

        // GetComponentInChildren<Text>().enabled = GetComponent<Button>().enabled = stageNumber > 1 && Statistics.Instance.levelCompleted[stageNumber - 2];

        // if (stageNumber == 1)
        //    GetComponentInChildren<Text>().enabled = GetComponent<Button>().enabled = true;
    }
}
