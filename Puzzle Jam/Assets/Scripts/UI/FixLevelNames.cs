﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FixLevelNames : MonoBehaviour
{
    private void Awake()
    {
        var textPool = GetComponentsInChildren<Text>();
        for (int i = 0; i < textPool.Length; i++)
        {
            textPool[i].text = "" + (i + 1);
        }

        var buttonPool = GetComponentsInChildren<Button>();
        var count = 0;
        foreach (var button in buttonPool)
        {
            count++;
            button.gameObject.name = "Button Stage " + count;
        }
    }
}
