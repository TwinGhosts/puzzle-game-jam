﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapColor : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            GameObject es = GameObject.Find("EventSystem");
            es.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        });
    }

    private void Update()
    {
        var g = GetComponent<Graphic>();
        if (g.color != Util.GameManager.GetCurrentColor()) g.color = Color.Lerp(g.color, Util.GameManager.GetCurrentColor(), Util.colorProgress);
    }
}
