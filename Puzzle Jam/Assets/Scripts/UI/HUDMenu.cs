﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HUDMenu : MonoBehaviour
{
    private FadeManager fader;
    public GameObject canvas;
    public GameObject winCanvas;

    private void Start()
    {
        fader = Util.FadeManager;
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        canvas.SetActive(false);
    }

    public void NextLevel()
    {
        fader.Fade(false);
        Time.timeScale = 1f;
        fader.OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LevelSelect()
    {
        fader.Fade(false);
        Time.timeScale = 1f;
        fader.OnFadeOut.AddListener(() => SceneManager.LoadScene("Level Select"));
    }

    public void Retry()
    {
        fader.Fade(false);
        Time.timeScale = 1f;
        fader.OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
    }

    public void Quit()
    {
        fader.Fade(false);
        Time.timeScale = 1f;
        fader.OnFadeOut.AddListener(() => SceneManager.LoadScene("Main Menu"));
    }
}
