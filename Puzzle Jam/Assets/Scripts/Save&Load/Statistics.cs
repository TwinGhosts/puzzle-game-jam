﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Statistics : MonoBehaviour
{
    public static Statistics Instance;

    public int currentLevel = 0;
    public bool[] levelCompleted = new bool[100];

    private void Awake()
    {
        if (!Instance)

        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        //Load();
    }

    public void SetLevel()
    {
        if (int.TryParse(SceneManager.GetActiveScene().name, out var level))
        {
            currentLevel = level;
        }
    }

    public void CompleteCurrentLevel()
    {
        levelCompleted[currentLevel] = true;
        //Save();
    }

    /*public void Save()
    {
        // Store all level data
        var count = 1;
        foreach (var levelCheck in levelCompleted)
        {
            SaveSystem.SetBool("level" + count + "completed", levelCompleted[count - 1]);
            count++;
        }

        SaveSystem.SaveToDisk();
    }*/

    /*public void Load()
    {
        // Load all level data
        var count = 1;
        foreach (var levelCheck in levelCompleted)
        {
            levelCompleted[count - 1] = SaveSystem.GetBool("level" + count + "completed", false);
            count++;
        }
    }*/
}
