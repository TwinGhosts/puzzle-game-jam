﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Node selectedNode = null;
    [HideInInspector] public List<Node> nodes = new List<Node>();
    [HideInInspector] public NodeConnector currentConnector = null;
    [HideInInspector] public Node nodeOnMouse = null;

    [HideInInspector] public Color currentColor = Color.white;
    private Color cameraTargetColor = Color.black;

    public Phase phase = Phase.White;

    [Header("Parents")]
    public Transform valueCanvas = null;
    public GameObject obstacleParent = null;

    [Header("Prefabs")]
    public NodeConnector connectorPrefab = null;
    public Text nodeValueTextPrefab = null;

    private void Awake ()
    {
        Util.GameManager = this;
    }

    private void Start ()
    {
        Statistics.Instance.SetLevel();
    }

    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space)) SwapColor();

        // Update the current connector for the selected node
        if (selectedNode && currentConnector != null)
        {
            currentConnector.line.SetPosition(0, selectedNode.transform.position);

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = selectedNode.transform.position.z;
            currentConnector.line.SetPosition(currentConnector.line.positionCount - 1, mousePos);
        }

        // When releasing the LMB, reset the selected node and connector
        if (Input.GetMouseButtonUp(0))
        {
            // Reset divisions
            if (selectedNode) selectedNode.divisions = 0;
            selectedNode = null;
            if (currentConnector && !currentConnector.node2)
                Destroy(currentConnector.gameObject);
            currentConnector = null;
        }

        // When RMB is pressed in a blank area and a node is selected, split
        if (selectedNode && currentConnector && (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.S)) && nodeOnMouse == null)
        {
            currentConnector.Divide();
        }

        // Change camera color
        Util.colorProgress += Time.deltaTime / Util.colorFadeTime;
        Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, cameraTargetColor, Util.colorProgress);

        if (Input.GetKeyDown(KeyCode.P))
        {
            Music.Instance.PlaySound(Music.Instance.e_button_click, 0.3f);
            Pause();
        }
    }

    public void Connect (Node node1, Node node2)
    {
        currentConnector.edge.enabled = false;

        // Line cast from node 1 to node 2
        var positionCount = currentConnector.line.positionCount;
        var objectList = new List<RaycastHit2D>();
        var blockedCast = false;

        // Loop through all edges of the connection
        for (int i = 0; i < positionCount - 1; i++)
        {
            // Get all of the hits for the current edge
            var hits = Physics2D.LinecastAll(currentConnector.line.GetPosition(i), currentConnector.line.GetPosition(i + 1));
            foreach (var hit in hits)
            {
                // Check whether the hit is an obstacle, we just need to hit one to break away
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    blockedCast = true;
                    break;
                }

                if (blockedCast) break;
            }
        }

        // When it isn't blocked by an obstacle and both nodes aren't full yet, connect
        if (!blockedCast && !node1.IsFull && !node2.IsFull)
        {
            // Set the connector nodes
            currentConnector.node1 = node1;
            currentConnector.node2 = node2;

            // Add the connection
            node1.currentConnections += 1;
            node2.currentConnections += 1;

            // Add the connector
            node1.connection = currentConnector;
            node2.connection = currentConnector;

            Music.Instance.PlaySound(Music.Instance.e_node_connect, 0.3f);

            // Set the end of the visual line to the released node
            var endPos = node2.transform.position;
            endPos.z = 0f;
            currentConnector.line.SetPosition(currentConnector.line.positionCount - 1, endPos);

            // Enable the line collision and update the collider to match the visuals
            currentConnector.edge.enabled = true;
            currentConnector.UpdateCollider();

            // Play particles
            currentConnector.node1.ps.Play();
            currentConnector.node2.ps.Play();

            // Reset the node info
            currentConnector = null;
            selectedNode = null;

            // Check for the win condition
            CheckWinCondition();
        }
        // When it is blocked, reset
        else
        {
            // Reset divisions
            selectedNode.divisions = 0;

            Music.Instance.PlaySound(Music.Instance.e_node_error, 0.3f);
        }
    }

    public void Disconnect (Node node)
    {
        if (!node || !node.connection) return;

        Music.Instance.PlaySound(Music.Instance.e_node_disconnect, 0.3f);
        node.connection.Disconnect();
    }

    public void SetNode (Node node)
    {
        currentConnector = Instantiate(connectorPrefab);
        currentConnector.node1 = node;
        currentConnector.targetColor = nodes[0].targetColor;
        selectedNode = node;
    }

    public void CheckWinCondition ()
    {
        foreach (var node in nodes)
        {
            if (!node.IsFull) return;
        }

        Music.Instance.PlaySound(Music.Instance.e_win, 0.3f);

        FindObjectOfType<HUDMenu>().winCanvas.SetActive(true);

        Statistics.Instance.CompleteCurrentLevel();

        foreach (var node in nodes)
        {
            node.GetComponent<Collider2D>().enabled = false;
        }
    }

    public void SwapColor ()
    {
        if (Camera.main.backgroundColor == Color.white || Camera.main.backgroundColor == Color.black)
        {
            var connectors = FindObjectsOfType<NodeConnector>();
            phase = (phase == Phase.Black) ? Phase.White : Phase.Black;

            // Swap connector colors
            foreach (var connector in connectors)
            {
                connector.SwapColor();

                var order = connector.line.sortingOrder;
                connector.line.sortingOrder = (order == 0) ? 1 : 0;
            }

            foreach (var node in nodes)
            {
                var order = node.GetComponent<SpriteRenderer>().sortingOrder;
                node.GetComponent<SpriteRenderer>().sortingOrder = (order == 0) ? 1 : 0;
            }

            foreach (var obstacle in FindObjectsOfType<NodeObstacle>())
            {
                var order = obstacle.GetComponent<SpriteRenderer>().sortingOrder;
                obstacle.GetComponent<SpriteRenderer>().sortingOrder = (order == 0) ? 1 : 0;
            }

            cameraTargetColor = GetOtherColor(Camera.main.backgroundColor);
            Util.colorProgress = 0;
        }
    }

    public void Pause ()
    {
        var menu = FindObjectOfType<HUDMenu>();

        if (!menu.canvas.activeSelf)
        {
            menu.canvas.SetActive(true);
            Time.timeScale = 0f;

            foreach (var node in nodes)
            {
                node.GetComponent<Collider2D>().enabled = false;
            }
        }
        else
        {
            foreach (var node in nodes)
            {
                node.GetComponent<Collider2D>().enabled = true;
            }
            menu.Resume();
        }
    }

    public Color GetCurrentColor ()
    {
        return (phase == Phase.White) ? Color.white : Color.black;
    }

    public Color GetInverseColor ()
    {
        return (phase == Phase.White) ? Color.black : Color.white;
    }

    private Color GetOtherColor (Color color)
    {
        return (color == Color.white) ? Color.black : Color.white;
    }
}

public enum Phase
{
    White,
    Black,
}