﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    [SerializeField]
    private Graphic _fadeGraphic = null;
    private Color _color;
    public bool _isFading = false;

    public UnityEvent OnFadeOut;
    public UnityEvent OnFadeIn;

    private void Awake()
    {
        Util.FadeManager = this;

        _fadeGraphic.gameObject.SetActive(true);
    }

    // Use this for initialization
    private void Start()
    {
        _color = _fadeGraphic.color;
        Fade(true);
    }

    public void Fade(bool fadeIn)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, 0.4f));
    }

    public void Fade(bool fadeIn, float duration = 0.4f)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, duration));
    }

    public void GoToParsedTextScene(Button button)
    {
        var text = button.GetComponentInChildren<Text>().text;
        GoToScene(text);
    }

    public void RestartScene(float duration = 0.4f)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }
    }

    public void NextScene(float duration = 0.4f)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
        }
    }

    public void GoToScene(string sceneName)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, 0.4f));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(sceneName));
        }
    }

    public void Quit(float duration = 0.4f)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => Application.Quit());
        }
    }

    private IEnumerator _Fade(bool fadeIn, float duration)
    {
        _isFading = true;

        var progress = 0f;
        var startColor = _fadeGraphic.color;
        var endColor = _color;
        endColor.a = (fadeIn) ? 0f : 1f;

        foreach (var button in FindObjectsOfType<Button>())
        {
            button.enabled = false;
        }

        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            _fadeGraphic.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        foreach (var button in FindObjectsOfType<Button>())
        {
            button.enabled = true;
        }

        _isFading = false;

        if (fadeIn)
            OnFadeIn.Invoke();
        else
            OnFadeOut.Invoke();
    }
}
