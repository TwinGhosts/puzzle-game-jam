﻿using UnityEngine;
using System.Collections;

public static class Util
{
    public static GameManager GameManager;
    public static NodeParent NodeParent;
    public static ConnectionParent ConnectionParent;
    public static FadeManager FadeManager;

    public static float colorFadeTime = 0.7f;
    public static float colorProgress = 0f;
}
