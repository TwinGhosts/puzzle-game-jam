﻿using UnityEngine;
using System.Collections;

public static class MathEx
{
    public static float ClampAngle (float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;

        if (angle > 360f)
            angle -= 360f;

        return Mathf.Clamp(angle, min, max);
    }

    public static float Approach (float Start, float End, float Shift)
    {
        return (Start < End) ? Mathf.Min(Start + Shift, End) : Mathf.Max(Start - Shift, End);
    }

    public static Vector2 VectorFromAngle (float dir)
    {
        return new Vector2(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
    }

    public static float AngleFromVector (Vector2 dir)
    {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public static float AngleFromPointToMouse (Vector2 point)
    {
        var direction = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - point;
        return AngleFromVector(direction);
    }

    public static float Clamp0360 (float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result;
    }

    public static float ClampMinus180Positive180 (float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result - 180f;
    }

    public static int ManhattanDistance (Vector2Int a, Vector2Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
        }
    }
}
