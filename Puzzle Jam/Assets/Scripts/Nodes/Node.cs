﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Node : MonoBehaviour
{
    [HideInInspector] public int currentConnections = 0;
    public int maxConnections = 1;
    [HideInInspector] public int divisions = 0;
    public int maxDivisions = 1;
    [HideInInspector] public NodeConnector connection = null;
    private SpriteRenderer sr;
    [HideInInspector] public Color targetColor = Color.white;
    [HideInInspector] public Color reverseTargetColor = Color.black;
    [HideInInspector] public Text valueText = null;
    public bool reverse = false;

    private bool mouseOnNode = false;
    [HideInInspector] public ParticleSystem ps;

    private void Awake ()
    {
        ps = GetComponentInChildren<ParticleSystem>();
        sr = GetComponent<SpriteRenderer>();

        sr.sortingOrder = (sr.color == Color.white) ? 1 : 0;

        var main = ps.main;
        main.startColor = sr.color;
    }

    private void Start ()
    {
        Util.GameManager.nodes.Add(this);

        // Create the text value object
        // valueText = Instantiate(Util.GameManager.nodeValueTextPrefab);
        // valueText.rectTransform.SetParent(Util.GameManager.valueCanvas);
        // valueText.rectTransform.position = Camera.main.WorldToScreenPoint(transform.position);

        transform.SetParent(Util.NodeParent.transform);
    }

    private void Update ()
    {
        /*
        targetColor = (reverse) ? Util.GameManager.GetInverseColor() : Util.GameManager.GetCurrentColor();
        reverseTargetColor = (reverse) ? Util.GameManager.GetCurrentColor() : Util.GameManager.GetInverseColor();

        if (sr.color != targetColor) sr.color = Color.Lerp(sr.color, targetColor, Util.colorProgress);
        */
        if (reverse)
            GetComponent<Collider2D>().enabled = Util.GameManager.phase == Phase.Black;
        else
            GetComponent<Collider2D>().enabled = Util.GameManager.phase == Phase.White;

        // if (valueText.color != reverseTargetColor) valueText.color = Color.Lerp(valueText.color, reverseTargetColor, Util.colorProgress);
        // valueText.text = "" + (maxDivisions - divisions);
    }

    public bool IsFull { get { return currentConnections == maxConnections; } }


    #region Mouse Controls
    private void OnMouseExit ()
    {
        mouseOnNode = false;
        if (Util.GameManager.nodeOnMouse == this) Util.GameManager.nodeOnMouse = null;
    }

    private void OnMouseOver ()
    {
        mouseOnNode = true;
        Util.GameManager.nodeOnMouse = this;

        if (Input.GetMouseButtonDown(0) && !IsFull)
            Util.GameManager.SetNode(this);

        if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.S))
            Util.GameManager.Disconnect(this);

        if (Input.GetMouseButtonUp(0) && Util.GameManager.selectedNode && Util.GameManager.selectedNode != this)
            Util.GameManager.Connect(Util.GameManager.selectedNode, this);
    }
    #endregion Mouse Controls
}