﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeConnector : MonoBehaviour
{
    public Node node1, node2;

    [Header("Components")]
    public LineRenderer line;
    public EdgeCollider2D edge;

    public Color targetColor = Color.white;

    private void Start()
    {
        transform.SetParent(Util.ConnectionParent.transform);

        line.startColor = line.endColor = node1.GetComponent<SpriteRenderer>().color;
        line.sortingOrder = node1.GetComponent<SpriteRenderer>().sortingOrder;
    }

    private void Update()
    {
        if (!Input.GetMouseButton(0) && node2 == null)
        {
            Destroy(gameObject);
        }

        /*
        if (line.startColor != targetColor)
        {
            line.startColor = Color.Lerp(line.startColor, targetColor, Util.colorProgress);
            line.endColor = Color.Lerp(line.startColor, targetColor, Util.colorProgress);
        }
        */
    }

    public void Divide()
    {
        if (node1.divisions < node1.maxDivisions)
        {
            line.positionCount += 1;

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0f;
            line.SetPosition(line.positionCount - 1, mousePos);

            Music.Instance.PlaySound(Music.Instance.e_node_divide, 0.4f);

            node1.divisions++;
        }
    }

    public void Disconnect()
    {
        node1.currentConnections -= 1;
        node2.currentConnections -= 1;

        node1.divisions = 0;

        Destroy(gameObject);
    }

    public void UpdateCollider()
    {
        // Define a new point array based on the existing line renderer
        Vector2[] points = new Vector2[line.positionCount];

        // Update collider based on the line renderer
        for (var i = 0; i < line.positionCount; i++)
        {
            points[i] = line.GetPosition(i) - gameObject.transform.position;
        }

        // Replace the edge points
        edge.points = points;
    }

    public void SwapColor()
    {
        // targetColor = (targetColor == Color.white) ? Color.black : Color.white;
    }
}
